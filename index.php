<?php get_header(); ?>

<!--<div class="container">-->
<div>
<!-- se ho cercato nella barra della ricerca qualcosa, allora il titolo della pag home sarà: Results: e il nome messo nela ricerca($s)-->
  <?php if(is_search()){  ?>

    <div class="container-fluid">
      <div class="row">
          <div class="col-12 pt-5 pb-3 row-page text-center mb-5" >
                <h2><?php esc_html_e('Results for:', 'nxcquadro'); ?> </h2>
                  <h1><?php echo $s; ?></h1>
            </div>
      </div>
    </div>


  <!-- se invece sono nella pagina delle categorie o dei post, mostrami come titolo della home la categoria o il tag.-->
  <?php } else if(is_category() || is_tag()) { ?>

    <div class="container-fluid">
      <div class="row">
          <div class="col-12 pt-5 pb-3 row-page text-center mb-5" >
              <h2>Post per la categoria:</h2>
              <h1> <?php echo single_cat_title();?></h1>
            </div>
      </div>
    </div>

    <?php  } else { ?>
<!-- altirmenti il titolo della pag home sarà la descrizione del sito impostata nella dashboard-->
      <!--<h1><?//php bloginfo('description');?></h1>-->

      <div class="container-fluid">
        <div class="row">
            <div class="col-12 pt-5 pb-3 row-page text-center mb-5" >
                  <h1 class="title-page display-4">Blog</h1>
              </div>
        </div>
      </div>

  <?php } ?>

  <!--<hr>-->
</div>

<main class="container">

  <div class="row"> <!--RIGA BOOTSTRAP. sARà DIVISA IN 2 COLONNE, UNA DA 8 SPAZI (col-sm-8) E UNA DA 4 SPAZI (col-sm-4)-->
<!--questa colonna contiene i post-->
        <div class="col-lg-8 mt-5">

              <!--INIZIO LOOP PER I POST-->
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <article <?php post_class();?>> <!--serve a inserire in automatico delle classi che useremo per stilizare i singoli articoli, come la classe body_class()-->




                  <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('nxcquadro_single', array('class' => 'img-fluid mb-4', 'alt'=> get_the_title())); ?>
                  </a>
                                                                          <!-- serve a inserire l'immagine. Alla funzione passao 2 parametri,
                                                                          1)lo slug dell'immagine che ho definito dentro functions.php,
                                                                          2) array a cui dico che la classe da applicare allo stile è la classe
                                                                          bootstrap img-fluid così l' immagine è responsive, e poi gli passo
                                                                          l'alt dell'immagine che sarà uguale al titolo del post-->
                  <h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2><!--è il primo post di default che wordpress crea in automatico. Questa funzione visualizzerà il titolo-->
                  <p> <?php the_time('j M Y');?> - <?php the_category(', ');?></p><!-- serve a visualizzare la data di pubblicazione e la categoria-->
                  <?php the_excerpt();?> <!--sono le ultime righe dell'articolo-->

              </article>

            <hr class="my-5">

          <?php endwhile;?>

          <!--serve a mettere il numero delle pagine nel caso in cui ho molti contenuti. Ad esempio nella pagina blog dove ho tanti articoli-->
              <div class="pagination">
                    <?php
                    global $wp_query;

                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                    	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    	'format' => '?paged=%#%',
                    	'current' => max( 1, get_query_var('paged') ),
                    	'total' => $wp_query->max_num_pages
                    ) );
                    ?>

              </div>


          <?php else: ?>
                <p><?php esc_html_e('Sorry, no post match your criteria.', 'nxcquadro'); ?></p>
              <?php endif; ?>
              <!--FINE LOOP PER I POST-->

        </div>

<!-- questa colonna contiene la mia sidebar-->
        <?php get_sidebar(); ?>

  </div>

</main>

<?php get_footer(); ?>
