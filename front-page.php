<?php get_header();?>

<div class="main-content">

<!--****************************************************
              Slider
************************************-->

<!-- Slider: codice preso dall lezione #precorso2. In particolare preso dal sito di bootstrap -->
 <section id="carouselExampleIndicators" class="carousel slide slider-big" data-ride="carousel">

      <div class="carousel-inner">
        <!---QUESTA QUI SOTTO Ã¨ UNA QUERY (LOOP) CHE SERE A MOSTRARE NELLO SLIDER GLI ULTIMI ARTIOLI DEL BLOG CHE HANNO UNA DETERMINATA CATEGORA.
     IL NOME DELLA CATEGORIA GLIELA STO PASSANDO DENTRO LA FUNZIONE WP_QUERY() -->

        <?php

        $nxcquadro_counter = 0;/* Ã¨ una ariabile utilizzata per assegnare solo alla prima immagine dello slider, la classe active, altrimenti lo slider non scorre*/

/*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere i contenuti dello SLIDER
dalle pagine che hano tassonomia uguale ad 'home visibility' con categoria uguala a slider.
Queste tassonomie le creo con un plugin chiamato CUSTOM POST TYPE UI e le definisco nella pagina di wordpress che oglio visualizzare*/

        $args = array(
        	'post_type' => 'page',
        	'tax_query' => array(
        		array(
        			'taxonomy' => 'home_visibility',
        			'field' => 'slug',
        			'terms' => 'slider'
        		)
        	)
        );


        // La Query
        $nxcquadro_the_query = new WP_Query( $args );

        // Il Loop
        while ( $nxcquadro_the_query->have_posts() ) :
         $nxcquadro_the_query->the_post(); ?>


          <?php $nxcquadro_counter++; ?>

            <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_big');?>
                                              <!-- Quest'ultimo pezzo di codice serve a stampare a video l'immagine, quello che farÃ 
                                                   e stampare nell'html solo l'url dell'immagine, senza usare il tag img.
                                                   Alla funzione passo 2 parametri
                                                   1)l'id dell'immagine
                                                   2) la dimensione della foto che ho definito nel file functions.php
                                                   Tutto questo codice lo ha inserito in una ariabile $image_attributes.
                                                   Questa variabile conterrÃ  tutte le variabili della mia immagine. Quella in poszione 0 Ã¨ l
                                                   l'url dell'immagine-->


            <div class="carousel-item <?php if($nxcquadro_counter == 1){ echo 'active'; } ?>" style="background: linear-gradient(rgba(0,0,0, 0.1), rgba(0,0,0, 0.1)), url(<?php echo $nxcquadro_image_attributes[0]; ?>); background-size: cover; background-position: center center">

                                                                                                      <!-- background-position: center center/// per centrare verticalmente e orizzontalmente l'immagine dentolo slider-->
                                                                                                   <!--linear-gradient(rgba(0,0,0, 0.3), rgba(0,0,0, 0.7)), serve per creare una sfumatura sopra l'immagine. In questo caso il colore messo Ã¨ un nero-->
                <div class="carousel-caption text-left margin-bottom"> <!--  <div class="carousel-caption d-none d-md-block">--><!--d-none sarebbe display-none. d-md-block questo vuol dire che il testo verrÃ  mostrato solo per dipositii grandi come pc. nei tablet e telefonini non verrÃ  mostrato. Se oglio che il testo ci deve essere sempre, deo eliminare queste 2 classi -->
                    <div class=container>
                        <!--<div class="trattino"></div>--> <!-- è il trattino sopra la scritta-->
                        <h3 class="text-center"><?php the_title(); ?></h3><!--titolo del post--><!-- la classe display-3 serve a impostre la dimesione del testo-->

                        <div class="carousel-text text-center">  <?php the_excerpt(); ?> </div><!--sono le ultime righe dell'articolo--><!-- la classe lead sere a impostare la dimensione del testo-->
                      <!--  <a href="<?php the_permalink(); ?>" class="btn btn-outline-light btn-lg"><?php esc_html_e('Guarda il video', 'nxcquadro')?></a>-->
                    </div>
                </div>
            </div>



        <?php endwhile;

        // Ripristina Query & Post Data originali
        wp_reset_query();
        wp_reset_postdata(); ?>

      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </section>

  <!-- FINE Slider: -->

<!--****************************************************
    SEZIONE "COSA FACCIO"
***********************************************************-->


  <section class="container">
    <div class="row">
          <div class="col-md-12 text-center mt-5">
              <h1 class="mt-5">Cosa Faccio</h1>
              <div class="trattino"></div>
          </div>

      </div>

      <div class="row">

              <?php
                  $args = array(
                  'post_type' => 'page',
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'home_visibility',
                      'field' => 'slug',
                      'terms' => 'in-evidenza'
                    )
                  )
                );

                  // La Query
                  $nxcquadro_the_query = new WP_Query( $args );  /*dentro WP_query gli passo i parametri che mi interessano, in questo caso,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a in-evidenza*/

                  // Il Loop
                  while ( $nxcquadro_the_query->have_posts() ) :

                    $nxcquadro_the_query->the_post();?>

                    <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_single');?>


                          <div class="col-md-4 col-xs-6 mt-3">
                            <div class="card card-cover" style="background: linear-gradient(rgba(0,0,0, 0.3), rgba(0,0,0, 0.8)), url(<?php echo $nxcquadro_image_attributes[0]; ?>); background-size: cover; background-position: center center">

                                     <div class="card-body text-center">
                                       <a href="<?php the_permalink(); ?>">
                                         <h4 class="card-title mt-5">  <?php the_title();?></h4><!--titolo del post-->
                                         <div class="card-text"><?php the_excerpt();?></div> <!--sono le ultime righe dell'articolo-->
                                         <p class="card-link"><?php esc_html_e('Scopri di più  ', 'nxcquadro')?><!--<i class="fa fa-angle-right" aria-hidden="true"></i>--></p>
                                       </a>
                                 </div>
                               </div>
                          </div>


                <?php endwhile;
                  // Ripristina Query & Post Data originali
                  wp_reset_query();
                  wp_reset_postdata();?>



                </div>
            </section>
    <!-- fine cards-->

    <!--****************************************************
                    Sezione Servizi
    ****************************************************-->
    <section class="container focuses mb-5">

      <div class="row">
            <div class="col-md-12 text-center mt-5">
                <h1 class="mt-5">Servizi</h1>
                <div class="trattino"></div>
            </div>

        </div>


        <div class="row mt-5">

            <?php
            $args = array(
            	'post_type' => 'page',
            	'tax_query' => array(
            		array(
            			'taxonomy' => 'home_visibility',
            			'field' => 'slug',
            			'terms' => 'focus'
            		)
            	)
            );


            // La Query
            $nxcquadro_the_query = new WP_Query( $args );/*dentro WP_query gli passo i parametri che mi interessano, in questo caso,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a focus*/

            // Il Loop
            while ( $nxcquadro_the_query->have_posts() ) :
             $nxcquadro_the_query->the_post(); ?>
             <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_single');?>

             <!-- div di bootstrap da 3 colonne, così creo 4 colonne con 3 spazi-->
             <div class="col-lg-3 focus">

               <img class="card-img-top img-fluid rounded-circle img-servizi mx-auto  d-block mb-3" src="<?php echo $nxcquadro_image_attributes[0]; ?>" alt="<?php the_title(); ?>">
               <h3 class="text-center mb-3"><?php the_field('intro_text'); ?></h3><!--sarebbe la paroline intro-text che si trova sopra ogni blocchetto dei focus-->
               <div class="text-intro text-center mb-5"><?php the_content(); ?></div><!--titolo del post--><!-- la classe display-3 serve a impostre la dimesione del testo-->


             </div>


            <?php endwhile;

            // Ripristina Query & Post Data originali
            wp_reset_query();
            wp_reset_postdata(); ?>
          </div>

    </section>


<!--****************************************************
              joombotron
************************************-->

<!--LOOP PER MOSTRARE IL CONTENUTO DI UNA PAGINA NEL JOOMBTRON-->
<?php

/*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere i contenuti della sezione
testimonials dalle pagine che hanno tassonomia uguale ad 'home visibility' con categoria uguala a cta-box.
Queste tassonomie le creo con un plugin chiamato CUSTOM POST TYPE UI, e le definisco nella pagina di wordpress che oglio visualizzare*/
$args = array(
  'post_type' => 'page',
  'tax_query' => array(
    array(
      'taxonomy' => 'home_visibility',
      'field' => 'slug',
      'terms' => 'cta-box'
    )
  )
);

  // La Query
  $nxcquadro_the_query = new WP_Query($args );  /*dentro WP_query gli passo i parametri che mi interessano, in questo caso, gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a cta-box*/

  // Il Loop
  while ( $nxcquadro_the_query->have_posts() ) :

    $nxcquadro_the_query->the_post();?>

    <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_big');?>
                                      <!-- Quest'ultimo pezzo di codice serve a stampare a video l'immagine, quello che farÃ 
                                      e stampare nell'html solo l'url dell'immagine, senza usare il tag img.
                                      Alla funzione passo 2 parametri
                                      1)l'id dell'immagine
                                      2) la dimensione della foto che ho definito nel file functions.php
                                      Tutto questo codice lo ha inserito in una ariabile $image_attributes.
                                     Questa variabile conterrÃ  tutte le variabili della mia immagine. Quella in poszione 0 Ã¨ l
                                     l'url dell'immagine-->




    <section class="jumbotron jumbotron-fluid mt-5 text-white call-to-action-box" style="background: linear-gradient(rgba(0,0,0, 0.6), rgba(0,0,0, 0.7)), url(  <?php echo $nxcquadro_image_attributes[0]; ?>); background-size: cover; background-position: center center">
    <!--url dell'immagine Ã¨ passato tramite il tag echo $nxcquadro_image_attributes[0]; cioÃ¨ sfruttando la ariabili $nxcquadro_image_attributes definita sopra, dove il suo prima valore, cioÃ¨ quello in posizione 0, Ã¨ proprio l'url dell'immagine-->

      <div class="container">
        <h3 class="cta-title mb-4"><?php the_title();?></p></h3><!--titolo del post-->
        <div class="cta-text mb-4"><?php the_excerpt();?></div>
        <a class="btn btn-primary" href="<?php the_permalink();?>" role="button"><?php esc_html_e('Scopri di più', 'nxcquadro'); ?></a>

      </div>
    </section>



    <?php endwhile;

      // Ripristina Query & Post Data originali
      wp_reset_query();
      wp_reset_postdata();?>


<!--FINE JOOMBTRON-->



<!--****************************************************
              Testimponials
************************************-->

<section class="container testimonials mt-5">
  <div class="row"> <!--creo una riga bootstrap diisa in 2 colonne. Una da 3 spazi e l'altra da 9-->

    <!--LOOP PER MOSTRARE IL CONTENUTO DI UNA PAGINA NELLA SEZIONE TESTIMONIALS-->
    <?php

    /*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere i contenuti della sezione
    testimonials dalle pagine che hanno tassonomia uguale ad 'home visibility' con categoria uguala a testimonials.
    Queste tassonomie le creo con un plugin chiamato CUSTOM POST TYPE UI, e le definisco nella pagina di wordpress che oglio visualizzare*/
    $args = array(
      'post_type' => 'page',
      'tax_query' => array(
        array(
          'taxonomy' => 'home_visibility',
          'field' => 'slug',
          'terms' => 'testimonials'
        )
      )
    );
      // La Query
      $nxcquadro_the_query = new WP_Query( $args );  /*dentro WP_query gli passo i parametri che mi interessano,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a testimonials*/

      // Il Loop
      while ( $nxcquadro_the_query->have_posts() ) :

        $nxcquadro_the_query->the_post();?>


            <div class="col-sm-3">
              <?php the_post_thumbnail('nxcquadro_quad', array('class' => 'img-fluid rounded-circle', 'alt'=> get_the_title())); ?>
                                                                      <!-- serve a inserire l'immagine. Alla funzione passao 2 parametri,
                                                                      1)lo slug dell'immagine che ho definito dentro functions.php,
                                                                      2) array a cui dico che la classe da applicare allo stile è la classe
                                                                      bootstrap img-fluid così l' immagine è responsive e anche la classe rounded-circle
                                                                      così l'immagine arà rotonda, e poi gli passo
                                                                      l'alt dell'immagine che sarà uguale al titolo del post-->

            </div>

            <div class="col-sm-9 mt-5">
                <blockquote>
                   <?php the_content();?><!--mostrerà tutto il contenuto della pagina, a differenza dell'excerpt che lo taglia-->
                </blockquote>
                <!--<?php the_field('second_content');?>-->
            </div>

          <?php endwhile;

          // Ripristina Query & Post Data originali
          wp_reset_query();
          wp_reset_postdata();?>



  </div>

</section>
<!--Fine testimonials-->

<!--****************************************************
    ULTIMI 3 POST
***********************************************************-->



    <section class="container">

      <div class="row">
            <div class="col-md-12 text-center mt-5">
                <h1>Ultime dal Blog</h1>
                <div class="trattino"></div>
            </div>

        </div>

      <div class="row">

        <?php

              /*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere gli ultimi 3 post*/
                $args = array(
                  'post_type' => 'post',
                  'posts_per_page' => 3
                );

                  // La Query
                  $nxcquadro_the_query = new WP_Query( $args );  /*dentro WP_query gli passo i parametri che mi interessano, in questo caso,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a in-evidenza*/

                  // Il Loop
                  while ( $nxcquadro_the_query->have_posts() ) :

                    $nxcquadro_the_query->the_post();?>

                    <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_single');?>


                      <div class="col-md-4 col-xs-6 mt-3">
                        <div class="card card-news" >

                              <div class="card-body">

                                    <?php the_post_thumbnail('nxcquadro_single', array('class' => 'img-fluid mb-4', 'alt'=> get_the_title())); ?>

                                            <a href="<?php the_permalink(); ?>"><h4 class="card-title">  <?php the_title();?></h4></a>
                                            <p class="card-meta"> <?php the_category(', ');?> | <?php the_time('j M Y');?></p>
                                            <div class="card-text"><?php the_excerpt();?></div>
                                            <p class="card-info"><?php esc_html_e('by: ', 'nxcquadro')?><?php the_author(); ?></p>

                          </div>
                        </div>
                    </div>









                <?php endwhile;
                  // Ripristina Query & Post Data originali
                  wp_reset_query();
                  wp_reset_postdata();?>

                  </div>
                </div>
            </section>
    <!-- fine 3 post-->





</div>

<?php get_footer();?>
