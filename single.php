<?php get_header();?>

<!--<div class="container-fluid">
  <div class="row">
      <div class="col-12 pt-5 pb-3 row-page text-center mb-5" >
            <h1 class="display-4 mb-5"><?php the_title();?></h1>
        </div>
  </div>
</div>-->

<div class="container">
  <div class="row">
      <div class="col-12 mt-5" >
            <h1 class="display-4 mb-2 titolo-post"><?php the_title();?></h1>
        </div>
  </div>
</div>

<main class="container mt-5">

  <div class="row"> <!--RIGA BOOTSTRAP. sARà DIVISA IN 2 COLONNE, UNA DA 8 SPAZI (col-sm-8) E UNA DA 4 SPAZI (col-sm-4)-->
<!--questa colonna contiene i post-->
        <div class="col-lg-8">

              <!--INIZIO LOOP PER I POST-->
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <article <?php post_class();?>> <!--serve a inserire in automatico delle classi che useremo per stilizare i singoli articoli, come la classe body_class()-->


                  <!--<h1 class="display-4 mb-5"><?php the_title();?></h1>-->
                  <p> <?php the_time('j M Y');?> - <?php the_category(', ');?></p><!-- serve a visualizzare la data di pubblicazione e la categoria-->
                  <?php the_post_thumbnail('nxcquadro_single', array('class' => 'img-fluid mb-4', 'alt'=> get_the_title())); ?>
                                                                          <!-- serve a inserire l'immagine. Alla funzione passao 2 parametri,
                                                                          1)lo slug dell'immagine che ho definito dentro functions.php,
                                                                          2) array a cui dico che la classe da applicare allo stile è la classe
                                                                          bootstrap img-fluid così l' immagine è responsive, e poi gli passo
                                                                          l'alt dell'immagine che sarà uguale al titolo del post-->

                  <?php the_content(); ?> <!-- è il testo dell'articolo-->

                  <?php wp_link_pages( 'pagelink=Page %'); ?><!-- sere ad avere in un articolo più pagine-->

                  <?php the_tags(); ?> <!-- è il tag  dell'articolo-->

                  <hr>
                  <!--commenti-->
                  <div class="comments">
                    <?php comments_template(); ?> <!--abilita i commenti-->
                  </div>

              </article>


              <?php endwhile; else: ?>
                <p><?php esc_html_e('Sorry, no post match your criteria.', 'nxcquadro'); ?></p>
              <?php endif; ?>
              <!--FINE LOOP PER I POST-->

        </div>

<!-- questa colonna contiene la mia sidebar-->
        <?php get_sidebar(); ?>

  </div>

</main>

<?php get_footer();?>
