


    <!--****************************************************
                  SEZIONE A 2 COLONNE
    ************************************-->

    <!--LOOP PER MOSTRARE IL CONTENUTO DI UNA PAGINA NELLA SEZIONE A 2 COLONNE SOTTO LO SLIDER-->
    <?php

    /*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere i contenuti della sezione a
    2 colonne dalle pagine che hanno tassonomia uguale ad 'home visibility' con categoria uguala a pre-evidenza.
    Queste tassonomie le creo con un plugin chiamato CUSTOM POST TYPE UI, e le definisco nella pagina di wordpress che oglio visualizzare*/
    $args = array(
      'post_type' => 'page',
      'tax_query' => array(
        array(
          'taxonomy' => 'home_visibility',
          'field' => 'slug',
          'terms' => 'pre-evidenza'
        )
      )
    );
      // La Query
      $nxcquadro_the_query = new WP_Query( $args );  /*dentro WP_query gli passo i parametri che mi interessano, in questo caso,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a pre-evidenza*/

      // Il Loop
      while ( $nxcquadro_the_query->have_posts() ) :

        $nxcquadro_the_query->the_post();?>


              <!-- 2 column TUTTO INSERITO MANUALMENTE TRANNE IL CODICE DEL BLOCKQUOTE CHE è STATO PRESO DA BOOTSTRAP-->
              <!-- creaiamo una struttura a 2 column. Per farlo creaiamo prima un container e poi una row (riga). Dentro la row metteremo le 2 colonne -->
              <section class="container two-columns">

                  <div class="row mt-5"> <!--la classe mt-5 (margin-top) questa è una classe bootstrap che serve mettere un margine di grandezza 5sopra il testo, altrimenti sarebbe attaccato allo slider-->
                    <!--siccome bootstrap è composto da 12 spazi (colonne), dovremmo creare 2 colonne da 6 spazi-->
                    <div class="col-sm-6"><!--cioè una colonna da 6 spazi, dove sm uol dire da tablet in su-->
                      <blockquote class="blockquote">
                        <h3 class="two-columns-title"><?php the_title();?></h3><!--titolo del post-->
                        <footer class="blockquote-footer"><?php esc_html_e('Author: ', 'nxcquadro')?><cite title="Source Title"><?php the_author(); ?></cite></footer> <!-- mi stamperà la string Author: e il nome dell'autore. cITE PRATICAMENTE STAMPA IN CORSIO-->
                      </blockquote>
                    </div>
                    <div class="col-sm-6"><!--cioè una colonna da 6 spazi, dove sm uol dire da tablet in su-->
                        <?php the_excerpt();?>
                    </div>
                  </div>
              </section>





        <?php endwhile;

          // Ripristina Query & Post Data originali
          wp_reset_query();
          wp_reset_postdata();?>

    <!-- FINE LOOP DELLE 2 COLONNE-->

<!--****************************************************
                Sezione con 2 Blockquote
************************************-->
<!--LOOP PER MOSTRARE IL CONTENUTO DI UNA PAGINA NELLA SEZIONE A 2 COLONNE SOTTO LO SLIDER-->
<?php

/*questo qui sotto è l'argomento che passerò alla query. Questo argomento dice di prendere i contenuti della sezione a
2 blockquote dalle pagine che hanno tassonomia uguale ad 'home visibility' con categoria uguala a post-evidenza.
Queste tassonomie le creo con un plugin chiamato CUSTOM POST TYPE UI, e le definisco nella pagina di wordpress che oglio visualizzare*/
$args = array(
  'post_type' => 'page',
  'tax_query' => array(
    array(
      'taxonomy' => 'home_visibility',
      'field' => 'slug',
      'terms' => 'post-evidenza'
    )
  )
);
  // La Query
  $nxcquadro_the_query = new WP_Query( $args );  /*dentro WP_query gli passo i parametri che mi interessano, in questo caso,  gli passo la ariabile $args che coincide con la pagina che ha home isibility uguale a post-evidenza*/

  // Il Loop
  while ( $nxcquadro_the_query->have_posts() ) :

    $nxcquadro_the_query->the_post();?>


          <!-- 2 column TUTTO INSERITO MANUALMENTE TRANNE IL CODICE DEL BLOCKQUOTE CHE è STATO PRESO DA BOOTSTRAP-->
          <!-- creaiamo una struttura a 2 column. Per farlo creaiamo prima un container e poi una row (riga). Dentro la row metteremo le 2 colonne -->
          <section class="container two-blockquote">

              <div class="row mt-5"> <!--la classe mt-5 (margin-top) questa è una classe bootstrap che serve mettere un margine di grandezza 5sopra il testo, altrimenti sarebbe attaccato allo slider-->
                <!--siccome bootstrap è composto da 12 spazi (colonne), dovremmo creare 2 colonne da 6 spazi-->
                <div class="col-sm-6"><!--cioè una colonna da 6 spazi, dove sm uol dire da tablet in su-->
                   <?php the_content();?><!--mostrerà tutto il contenuto della pagina, a differenza dell'excerpt che lo taglia-->
                </div>
                <div class="col-sm-6"><!--cioè una colonna da 6 spazi, dove sm uol dire da tablet in su-->
                    <?php the_field('second_content');?><!--sere a mostrare il campo second content. Questo second-content l'ho creato con il pluginADVAMCE CUSTOM FIELD e si personalizza all'interno delle pagine, e in questo caso coincide con i blockquote creati dentro la pagina chiamata "two blockquote"-->
                </div>
              </div>
          </section>





    <?php endwhile;

      // Ripristina Query & Post Data originali
      wp_reset_query();
      wp_reset_postdata();?>

<!-- FINE blocco con 2 Blockquote-->
