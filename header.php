<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="description" content="<?php bloginfo('description');?>">

  <?php wp_head();?> <!--per inserire gli stili dei plugin e del nostro tema-->
</head>
<body <?php body_class(); ?>>

<!--nav menu di tipo walker. Serve per collegare il menù di Wordpress con il mio-->

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary animate"> <!-- la classe animate serve per aniamre il colore del background della navbar-->
    <div class="container">
      <a class="navbar-brand animate" href="<?php echo esc_url_raw(home_url()); ?>"><img class="animate" src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="logo sito"></a><!--ho inserito tramite le best practis di Wordpress, il link alla home (echo es_url_row(home_url()); e come logo usa l'immagine dentro la cartella img. Per recuperare l'immagine usa il codice php get_template_directory_uri-->
       <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>

      <div class="collapse navbar-collapse" id="bs4navbar">
             <?php
             wp_nav_menu([
               'menu'            => 'header',
               'theme_location'  => 'header',
               'container'       => '',
               'container_id'    => '',
               'container_class' => '',
               'menu_id'         => false,
               'menu_class'      => 'navbar-nav mr-auto',
               'depth'           => 2,
               'fallback_cb'     => 'bs4navwalker::fallback',
               'walker'          => new bs4navwalker()
             ]);
             ?>

<!--codice preso da bootstrap 4 che serve a creare la casella di search-->
    <form class="form-inline my-2 my-lg-0" action="<?php echo esc_url_raw(home_url()); ?>" method="get"> <!--l'azione al form sarà l'url alla home cioè ?php echo esc_url_raw(home_url()); -->
         <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="s"> <!--name="s" serva a wordpress per sapere da dove arrivano le query di ricerca-->
         <button class="icon-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button> <!--icona del search di font awesome-->
    </form>

<!--questo qui sotto è il menu che contiene le icone social. Ha la stessa classe navbar-nav del menu, e in più ha la classe push-right per spostarlo a destra-->
             <ul class="navbar-nav navbar-social push-right">
               <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
               <li> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
               <li> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
             </ul>
        </div>
     </div>
   </nav>
