<?php

/* ___________________________________________________________________

Nav Walker Bootstrap
_____________________________________________________________________*/

// Register Custom Navigation Walker
require_once('assets/bs4navwalker.php');

/*__________________________________________________________________________

 content_width
___________________________________________________________________________*/

if ( ! isset( $content_width ) ) $content_width = 1400;

/*__________________________________________________________________________

 setup Theme
___________________________________________________________________________*/

if(! function_exists('nxcquadro_setup_theme()') ) {/*L'if sere per fare in modo che venga eseguita solamente se non esiste da un altra parte.*/

      function nxcquadro_setup_theme(){


          add_theme_support("title-tag");/*SUPPORT PER I TITLE. questa funzione serve per creare il supporto per i titoli nell'header in maniera automatica.
                                          In questo caso. la funzione serve per creare il tag title automantico nell'header*/

          add_theme_support( 'automatic-feed-links' );

          add_theme_support("post-thumbnails");/*serve ad attivare le thumbnails. Coì attio l'immagine in eidenza per gli articoli e le pagine*/
          add_image_size("nxcquadro_big", 1400, 800, true); /*serve per aggiungere una dimensione custom per l'immagine. Tutti i formati immagine
                                            hanno bisogno come prefisso il nome del nostro tema.
                                            In questo caso sto specificando la grandezza dell'immagine dello slider per questo l'ho
                                            chiamata _big. Poi specifico la larghezza e l'altezza.
                                            Con true dico che l'immagine deve essere croppata*/
          add_image_size("nxcquadro_quad_small", 400, 400, true);
          add_image_size("nxcquadro_quad", 600, 600, true);/*Creo un altro formato personale di tipo quadrato*/
          add_image_size("nxcquadro_single", 800, 500, true);/*creo un altro formato immagine personale per le news*/


          register_nav_menus(array( /*Serve ad attivare (registrare) il custom menu. Questa è una funzione che serve a creare un menù customizzabile, per poter usare il drag & drop di wordpress*/
          //'header' => 'Header',   /*custom menu per l'header. Scritto così non è adatta alle traduzioni, per cui è meglio scriverlo come sotto*/
          'header' => esc_html__('Header', 'nxcquadro'),/* Così, con la funzione esc_htl_ rendo il testo traducibile. Alla funzione passo 2
                                                          valori, il testo che voglio tradurre (header) e lo slug del tema di cui fa parte
                                                          la traduzione (cioè il nome del tema, in questo caso nxcquadro).
                                                          Questo si deve fare per qualsiasi voce voglio andare a tradurre.
                                                          Posso anche inserire un altra riga del tipo 'header' => esc_html_('Header', 'nxcquadro'),
                                                          cambiando le dichiarazioni, e usarla perfare il menu del footer*/
           ));

           //load cartella languages e quindi dei file pe la traduzione
           load_theme_textdomain('nxcquadro', get_template_directory().'/languages');
         }
}

/* Ora dobbiamo attivare tutti questi settaggi, per farlo usiamo le Azioni.*/
/*Noi useremo la funzione qui sotto che dice: dopo che è stato settato il tema (after_setup_theme) lancia questa funzione (nxcquadro_setup_theme)*/

add_action('after_setup_theme', 'nxcquadro_setup_theme');


/*_______________________________________________________________________

Funzione per inserire la sidebar quindi i widget
_________________________________________________________________________*/
if(! function_exists('nxcquadro_sidebars()') ) {

  function nxcquadro_sidebars(){

    register_sidebar(array(       /* i parametri passati sono:*/
      'name' => esc_html__('Primary', 'nxcquadro'),  //  1) il nome che dò alla sidebar (Primaruy);
      'id'=> 'primary',   //  2) l'id;
      'description' => esc_html__('Main Sidebar', 'nxcquadro'),  //3) la descrizione
      'before_title' =>'<h3>', //  4-5) che formato avrà il titolo di ogni widget, qui metto un h3
      'after_title' =>'</h3>',
      'before_widget' => '<div class="widget my-5 %2$s clearfix">', /*6-7) definiamo anche il contenitore per i widget,
                                                                    cioè un div che li conterrà, a cui sto passando la classes
                                                                     widget e un margine inferiore e superiore di 5, %2$s sere per
                                                                     stampare a video i nomi delle classi dei widget, mentre clearfix
                                                                     lo ha usato perchè nelle proe le spaziature sono collassate e così
                                                                     le ha sistemate*/
      'after_widget' => '</div>',)
    );
  }
}
//Lanciamo la funzione

add_action('widgets_init', 'nxcquadro_sidebars');

/*_______________________________________________________________________

Include javascript files
_________________________________________________________________________*/

if(! function_exists('nxcquadro_scripts()') ) {

      function nxcquadro_scripts(){

          wp_enqueue_script('nxcquadro-popper-js', get_template_directory_uri().'/js/popper.min.js', array('jquery'), null, true);
          wp_enqueue_script('nxcquadro-bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), null, true);
                                                      /*serve per includere i javascript. I parametri passati sono:
                                                      1) lo slug dello javascript
                                                      2) il percorso dove si trova il file javascript. Prima di
                                                      specificare il percorso, prendo le cartelle css e js creati nella lezione
                                                      precorso2 e le sposto nella cartella del mio tema. Quindi userò la funzione
                                                      get_template_directory_uri() per indicargli la radice del percorso
                                                      e dopo potrò specificare il percorso preciso.
                                                      3) Questo parametro serve a specificare che questo bootstrap
                                                      è una dipendenza di jquery
                                                      4) La versione di jquery = null
                                                      5) true per dirfli di isualizzarlo nel footer. Se avessi messo false voleva dire di
                                                      isualizzarlo nell'header*/
         wp_enqueue_script('nxcquadro-scripts-js', get_template_directory_uri().'/js/scripts.js', array('jquery'), null, true);/*è lo scripts che mi serve per fare il modo che il menu sotto i 100px si colori*/

        if ( is_singular() ) wp_enqueue_script( "comment-reply" );   //inserirà uno script per i commenti all'interno della pgina singola

      }

}

/* Anche qui per lanciare la funzione devo usare un azione.Qui l'azione è wp_enqueue_scripts*/

add_action('wp_enqueue_scripts', 'nxcquadro_scripts');

/*___________________________________________________________________________________

 Include css files
___________________________________________________________________________________*/

//COME PER GLI SCRIPT


if(! function_exists('nxcquadro_styles()') ) {

      function nxcquadro_styles(){

          wp_enqueue_style('nxcquadro-bootstrap-css', get_template_directory_uri().'/css/bootstrap.min.css');
          wp_enqueue_style('nxcquadro-style-default-css', get_template_directory_uri().'/style.css');//sto inserendo il mio file style.css
          wp_enqueue_style('nxcquadro-font-awesome', get_template_directory_uri().'/css/font-awesome.min.css');//sto inserendo il  file css di fontawesome
          wp_enqueue_style('nxcquadro-font', '//fonts.googleapis.com/css?family=Montserrat|Raleway:300,400,700');//è il font monserrat preso da google font. I numeri indicano i pesi



        }

}
add_action('wp_enqueue_scripts', 'nxcquadro_styles');


/*___________________________________________________________________________________

Filtro per modificare i valori di defaults della sezione commenti(al bottone)
___________________________________________________________________________________*/

function wpdocs_comment_form_defaults( $defaults ) {
  $defaults['class_submit'] ="btn btn-primary btn-lg";
  return $defaults;
}
add_filter( 'comment_form_defaults', 'wpdocs_comment_form_defaults' );


/*___________________________________________________________________________________

Questa funzione serve a settare la classe navbar-transparent (cioè ad avere il menu trasparente)
solo se ho un immagine di copertina && sono in una pagina, oppure se sono nella home-
Quindi nella altre pagine il menu sarà di default quello azzurro di bootstrap
___________________________________________________________________________________*/

add_action( 'body_class', 'add_class_bg_trasp_body');

function add_class_bg_trasp_body($classes){ /* alla funzionegli passo le classi di default $classes*/


  if (is_front_page()){

    array_push($classes,'navbar-transparent');  /* se sono nella home cioè nella (is_front_page), allora aggiungi la classe navbar-trasparent (per impostare il menu trasparente) alla home*/

  }
else{

  array_push($classes,'navbar-colored');
}
  return $classes;
}


 ?>
