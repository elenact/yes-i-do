<?php /* Template Name: Pagina Contatti*/ ?>
<?php get_header();?>

<!-- esegui il codice della jombtroon solo se c'è un immagine di copertina-->
  <?php if(has_post_thumbnail()){ ?>


            <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_big');?>


            <section class="jumbotron jumbotron-fluid jumbotron-page text-white call-to-action-box" style="background: linear-gradient(rgba(0,0,0, 0.8), rgba(0,0,0, 0.7)), url(  <?php echo $nxcquadro_image_attributes[0]; ?>); background-size: cover; background-position: center center">
            <!--url dell'immagine Ã¨ passato tramite il tag echo $nxcquadro_image_attributes[0]; cioÃ¨ sfruttando la ariabili $nxcquadro_image_attributes definita sopra, dove il suo prima valore, cioÃ¨ quello in posizione 0, Ã¨ proprio l'url dell'immagine-->

              <div class="container">
                <h1 class="cta-title"><?php the_title();?></p></h1><!--titolo del post-->
              </div>
            </section>

  <?php } ?>

<main >
  <!--INIZIO LOOP PER I POST-->
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <article <?php post_class();?>> <!--serve a inserire in automatico delle classi che useremo per stilizare i singoli articoli, come la classe body_class()-->

                    <!-- l'if qui sotto serve a specificare che se gia c'è un immagine d thmbanial, cioè se c'è un immagine di copertina(che si
                    imposta con il codice jumbotron qui sopra), allora non visualizzare il titolo, altrimenti se la foto copertina non c'è
                    allora visualizza il titolo-->

                    <?php if(has_post_thumbnail()){} else { ?>
                      <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 pt-5 pb-3 row-page text-center" >
                                  <h1 class="title-page"><?php the_title();?></h1>
                              </div>
                        </div>
                      </div>

                    <?php } ?>

                      <div class="container mt-5">
                        <div class="row">

                            <div class="col-md-4 align-self-center">

                              <div class="list-group larghezza-info">

                                  <div class="d-flex w-100 justify-content-between ">
                                          <h4 class="mb-3 info-title"><i class="fa fa-phone fa-2x fontawesome-contatti"></i>Telefono</h4>
                                  </div>
                                  <div class="mb-1 info" style="margin-left:52px"><?php the_field('telefono'); ?></div>

                                  <div class="d-flex w-100 justify-content-between mt-5">
                                          <h4 class="mb-3 info-title"><i class="fa fa-envelope fa-2x fontawesome-contatti"></i>E-mail</h4>
                                  </div>
                                  <div class="mb-1 info" style="margin-left:62px"><?php the_field('email'); ?></div>

                                <!--  <div class="d-flex w-100 justify-content-between mt-5">
                                          <h5 class="mb-1">List group item heading</h5>
                                  </div>
                                  <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>-->


                              </div>
                            </div>

                          <div class="col-md-8 mt-5 ">

                                <h1 class="text-center">  <?php the_field('intro_text');?></h1>
                                <div class="trattino mx-auto"></div>
                                  <?php the_content();?>
                                  <?php the_field('modulo_contatti');?>

                          </div>
                        </div>
                      </div>




              </article>


              <?php endwhile; else: ?>
                <p><?php esc_html_e('Sorry, no post match your criteria.', 'nxcquadro'); ?></p>
              <?php endif; ?>
              <!--FINE LOOP PER I POST-->



</main>

<?php get_footer();?>
