<footer class="footer bg-dark text-white mt-4"> <!--uso le classi bootstrap per indicare sfondo scuro e testo bianco e margin top di 4-->

    <div class="container py-4">
      <p class="mb-0">
        <?php esc_html_e('Copyright', 'nxcquadro')?> <?php echo date('o');?> - <?php bloginfo('name')?></p>
        <!-- ho inserito il testo Copyright (con la funzione esc_htm_e così sarà traducibile ), l'anno con a funzione date() e il nome del nostro tema-->
    </div>

</footer>


  <?php wp_footer();?> <!-- per inserire tutti gli script dei plugin e del tema-->
</body>
</html>
