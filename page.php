<?php /* Template Name: Pagina Marchetti*/ ?>
<?php get_header();?>

<!-- esegui il codice della jombtroon solo se c'è un immagine di copertina-->
  <?php if(has_post_thumbnail()){ ?>


            <?php $nxcquadro_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nxcquadro_big');?>
                                              <!-- Quest'ultimo pezzo di codice serve a stampare a video l'immagine, quello che farÃ 
                                              e stampare nell'html solo l'url dell'immagine, senza usare il tag img.
                                              Alla funzione passo 2 parametri
                                              1)l'id dell'immagine
                                              2) la dimensione della foto che ho definito nel file functions.php
                                              Tutto questo codice lo ha inserito in una ariabile $image_attributes.
                                             Questa variabile conterrÃ  tutte le variabili della mia immagine. Quella in poszione 0 Ã¨ l
                                             l'url dell'immagine-->




            <section class="jumbotron jumbotron-fluid text-white call-to-action-box" style="background: linear-gradient(rgba(0,0,0, 0.6), rgba(0,0,0, 0.7)), url(  <?php echo $nxcquadro_image_attributes[0]; ?>); background-size: cover; background-position: center center">
            <!--url dell'immagine Ã¨ passato tramite il tag echo $nxcquadro_image_attributes[0]; cioÃ¨ sfruttando la ariabili $nxcquadro_image_attributes definita sopra, dove il suo prima valore, cioÃ¨ quello in posizione 0, Ã¨ proprio l'url dell'immagine-->

              <div class="container">
                <h1 class="cta-title"><?php the_title();?></p></h1><!--titolo del post-->
              </div>
            </section>

  <?php } ?>

<main class="container mt-5">

  <div class="row"> <!--RIGA BOOTSTRAP. sARà DIVISA IN 2 COLONNE, UNA DA 8 SPAZI (col-sm-8) E UNA DA 4 SPAZI (col-sm-4)-->
<!--questa colonna contiene i post-->
        <div class="col-sm-8 ml-sm-auto mr-sm-auto"><!--ml-sm-auto mr-sm-auto comando bootst per centrare la colonna da 8 spazi. -mr-sm- auto sarebbe margin right sulla colonno sm uguale d auto-->

              <!--INIZIO LOOP PER I POST-->
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <article <?php post_class();?>> <!--serve a inserire in automatico delle classi che useremo per stilizare i singoli articoli, come la classe body_class()-->

                    <!-- l'if qui sotto serve a specificare che se gia c'è un immagine d thmbanial, cioè se c'è un immagine di copertina(che si
                    imposta con il codice jumbotron qui sopra), allora non visualizzare il titolo, altrimenti se la foto copertina non c'è
                    allora visualizza il titolo-->

                    <?php if(has_post_thumbnail()){} else { ?>
                      <h1 class="display-4 mb-5 text-center"><?php the_title();?></h1><!--è il primo post di default che wordpress crea in automatico. Questa funzione visualizzerà il titolo-->
                    <?php } ?>

                  <?php the_content(); ?> <!-- è il testo dell'articolo-->



              </article>


              <?php endwhile; else: ?>
                <p><?php esc_html_e('Sorry, no post match your criteria.', 'nxcquadro'); ?></p>
              <?php endif; ?>
              <!--FINE LOOP PER I POST-->

        </div>

  </div>

</main>

<?php get_footer();?>
