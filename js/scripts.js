/*Il mio menù è trasparente. Con questa funzione il menu si colorerà quando scrollo di 100px*/
(function ($){
  var nxcquadro_scroll_position=0;
    $(document).scroll(function(){

      nxcquadro_scroll_position=$(this).scrollTop();

        if(nxcquadro_scroll_position>100){
          $("body").addClass('is-scrolled');
        }else{
          $("body").removeClass('is-scrolled');

        }

    });

}(jQuery));
